﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_NIS_MimoLAN
{
    public class NISMimoLAN
    {
        public string Pocitac { get; set; }
        public string IP { get; set; }
        public string Pozn1 { get; set; }
        public string Pozn2 { get; set; }
        public string Pozn3 { get; set; }
          public bool MimoLAN { get; set; }
    }
}
