﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_NIS_MimoLAN
{
    public class FileReader
    {
        public List<NISMimoLAN> MimoLAN()
        {

            List<NISMimoLAN> topologie = new List<NISMimoLAN>();
            var fileLocationWithName = ConfigurationManager.AppSettings["FileLocation"] + ConfigurationManager.AppSettings["FileName"];

            try
            {



                using (var reader = new StreamReader(fileLocationWithName))
                {


                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(Convert.ToChar(ConfigurationManager.AppSettings["FileSplitter"]));
                        Console.WriteLine(values[0] + "-" + values.Count());
                        switch (values.Count())
                        {
                            case 3:
                                topologie.Add(new NISMimoLAN()
                                {
                                    Pocitac = values[0],
                                    IP = values[1],
                                    Pozn1 = values[2],
                                    MimoLAN = true,
                                });
                                break;
                            case 4:
                                topologie.Add(new NISMimoLAN()
                                {
                                    Pocitac = values[0],
                                    IP = values[1],
                                    Pozn1 = values[2],
                                    Pozn2 = values[3],
                                    MimoLAN = true,
                                });
                                break;
                            case 5:
                                topologie.Add(new NISMimoLAN()
                                {
                                    Pocitac = values[0],
                                    IP = values[1],
                                    Pozn1 = values[2],
                                    Pozn2 = values[3],
                                    Pozn3 = values[4],
                                    MimoLAN = true,
                                });
                                break;
                        } 
                    }
                }
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["FileWithHeader"]))
                {
                    topologie.RemoveAt(0);
                }


                var folderProcessed = Directory.CreateDirectory(ConfigurationManager.AppSettings["FileLocation"] + @"\Processed");
                File.Move(fileLocationWithName, folderProcessed.FullName + @"\DoneLikv" + DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss") + ".csv");


                return topologie;
            }
            catch (Exception ex)
            {
                var folderLogs = Directory.CreateDirectory(ConfigurationManager.AppSettings["FileLocation"] + @"\Logs");
                using (var writter = File.AppendText(folderLogs.FullName + @"\Logs " + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                {
                    writter.Write(string.Format("{0} - {1}", DateTime.Now.ToString(), ex.Message));
                    writter.WriteLine();
                }
                return null;
            }
        }
    }
}
