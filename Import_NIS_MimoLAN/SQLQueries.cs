﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_NIS_MimoLAN
{
   public class SQLQueries
    {
        private static readonly string connString = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        public bool SaveToDB(List<NISMimoLAN> NisRows)
        {
            try
            {

                using (SqlConnection connection = new SqlConnection(connString))
                {


                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText = "use KIS";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "TRUNCATE TABLE KIS_MimoLAN";
                        cmd.ExecuteNonQuery();
                    }
                    using (SqlCommand command = new SqlCommand("INSERT INTO KIS_MIMOLAN ([ComputerName],[IP] ,[Descr1], [Descr2],[Descr3], [MimoLAN])" +
                                       " VALUES (@ComputerName,@IP,@Descr1,@Descr2,@Descr3,@MimoLAN)", connection))
                    {
                        foreach (var NisRow in NisRows)
                        {
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@ComputerName", NisRow.Pocitac ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@IP", NisRow.IP ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@Descr1", NisRow.Pozn1 ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@Descr2", NisRow.Pozn2 ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@Descr3", NisRow.Pozn3 ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@MimoLAN", NisRow.MimoLAN);
                            command.ExecuteNonQuery();
                        }
                    }
                }
                return true;





            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}
