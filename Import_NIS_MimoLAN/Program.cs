﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_NIS_MimoLAN
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileReader = new FileReader();
            var rows = fileReader.MimoLAN();
            var recorder = new SQLQueries();
            bool res = recorder.SaveToDB(rows);
        }
    }
}
